var searchData=
[
  ['score_5fbetween_5ftr_56',['score_between_tr',['../namespaceutils_d_t_w.html#aeb74b9c3fd67f4042ebaf73957a9ad27',1,'utilsDTW']]],
  ['score_5fbetween_5ftrain_57',['score_between_train',['../namespaceutils_d_t_w.html#a6ee8cb534b4eac06b4031f002c4faec0',1,'utilsDTW']]],
  ['score_5fbetween_5ftrain_5fand_5ftest_58',['score_between_train_and_test',['../namespaceutils_d_t_w.html#ae2b14373c67d06acabb8c2b87a215c3d',1,'utilsDTW']]],
  ['score_5ftest_5ffile_59',['score_test_file',['../namespaceutils_d_t_w.html#aaf560a72b4b3dd70e389047e56634bcb',1,'utilsDTW']]],
  ['score_5ftest_5ffolder_60',['score_test_folder',['../namespaceutils_d_t_w.html#ac18d27723d2f547d330966ddfe2322ef',1,'utilsDTW']]],
  ['size_5fkeyword_61',['size_keyword',['../namespacereal__time.html#aecede7e694b6d167fc0292d2fa805f3c',1,'real_time']]],
  ['stream_62',['stream',['../namespaceplay__file.html#ae489866f8220882799d2786ca0690e5b',1,'play_file.stream()'],['../namespacerecord__file.html#ae489866f8220882799d2786ca0690e5b',1,'record_file.stream()']]]
];
