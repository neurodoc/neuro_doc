var searchData=
[
  ['recognition_5ffile_98',['recognition_file',['../namespaceutils_d_t_w.html#ab3ba813813d2e5ab0b8b4327e5571d68',1,'utilsDTW']]],
  ['recognition_5ftest_99',['recognition_test',['../namespaceutils_d_t_w.html#a21f8cfc9d2a8a44818bd22595aa0df38',1,'utilsDTW']]],
  ['remove_5fsilence_100',['remove_silence',['../namespacefeatures.html#aa0dd2f81a1994fb1b78766a404642ee3',1,'features']]],
  ['run_101',['run',['../classreal__time_1_1recorder.html#ad22709b2e67308af35f55680d5a026e0',1,'real_time.recorder.run()'],['../classreal__time_1_1process.html#ad22709b2e67308af35f55680d5a026e0',1,'real_time.process.run()'],['../classrealtime__v2_1_1recorder.html#ad22709b2e67308af35f55680d5a026e0',1,'realtime_v2.recorder.run()'],['../classrealtime__v2_1_1process.html#ad22709b2e67308af35f55680d5a026e0',1,'realtime_v2.process.run()'],['../classrealtime__v3_1_1recorder.html#ad22709b2e67308af35f55680d5a026e0',1,'realtime_v3.recorder.run()'],['../classrealtime__v3_1_1process.html#ad22709b2e67308af35f55680d5a026e0',1,'realtime_v3.process.run()']]]
];
